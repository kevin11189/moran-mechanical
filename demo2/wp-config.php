<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'morandemo2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(t-|(18fk;=WU]8Gn$X6n8t3}q{:txloXPoCHm6ZdO?D)GQHQ5EQcV3JF++C EaU');
define('SECURE_AUTH_KEY',  'y!{&Y[gTI^VarW4282`7H.,ymPZz{2@]ZDfC3~uqx]cwQV[;3}T!45y)NQs*->[D');
define('LOGGED_IN_KEY',    '.)DAD,E]Vi#e-a>fnw=nfH +4>!)s+j+hP;w^.T{6VIZt}uNb9<S|uw#JB{0o%%F');
define('NONCE_KEY',        'Hxuo^^2+hT&kX!cW&4z+fs?Q-[kYDG~c;?kEP!t,-]Iei}=UlI_e.X!o?CHJsClf');
define('AUTH_SALT',        'u#Wlk)oA<~T.;| ?L#Lz-??7+yJw=,{y7)aCj$D@74T8Se|{Yzf>&#@<86HL??d&');
define('SECURE_AUTH_SALT', 'WaXBa3K )O{0Ed14m!L>,VpUV-mKG*/NXu )|JxG:%e|B<XO7c9!Xk7 <(MCx)E@');
define('LOGGED_IN_SALT',   'X<EMMW_SYg:hr?)<mGf{e ~j}P4O92aP}Xd>w-kZ*aP-rw=@3EFhi|`3&^?Q89e?');
define('NONCE_SALT',       'WrgaqKEc^[4b)a5,J3uEPCp`R,}Q;)T~:e[tI67}r$7[m[LoP:bKrf*Nlgb^}#.4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
