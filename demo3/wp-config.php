<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'morandemo3');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E|*<#|gm@`=q4fP|B3LBuRRu&JQsT-eQEOMa0M[YNFaT)>o&l/,K1H4|u~Fwz.|F');
define('SECURE_AUTH_KEY',  'O&|=r4<22fu~XC3JMb(%Ij<LrPc~*S9{DxWXX;YF!P[HY?-7T`cgTR5QX!(~W8E}');
define('LOGGED_IN_KEY',    'kPBsF2!?O[?m8z:?B<Cf9q^@+~R?(z|MO&I5{>{cIGMY4%Gi)Pax.qpyIZ o Cd^');
define('NONCE_KEY',        '(~I;<Y_y-7c7S+7c:4o-)q Uqm5@JDz[-q)e4_C{N6qk4Ftf[+,Q&5R4iI}kvv&C');
define('AUTH_SALT',        '#(%RkC,e9+O6kt#m@S[8wKH@NctN^gG}L&1nXq{+isI(7S]o^6>wiv~|1g@yqYt#');
define('SECURE_AUTH_SALT', '?11swO)#4jVbj/o@#C6~^,r<DRuQ+p(x8*?f^3W^)|]o4v+DQ6-Ro55x!jv>c!0-');
define('LOGGED_IN_SALT',   '}j=N}1~Xw9^,#Cy^VM6AK@NaQd]+HGsn|rl/ChV`C|IQQ5|55e.+?E}:to6p+D!C');
define('NONCE_SALT',       'V.:v-)T|M2hJBN|V{3|S $|#`omokQb9[N{#H+Nz7dV&f,YWQn-fx]ZgX,ercXLC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
