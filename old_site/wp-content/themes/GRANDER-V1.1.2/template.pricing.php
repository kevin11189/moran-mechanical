<?php 
/*
 * Template Name: Pricing
 */
get_header(); ?>

<div class="container">
                <div class="page-title-heading">
                    <h2><?php the_title(); ?><?php if ( !get_post_meta($post->ID, 'snbpd_pagedesc', true)== '') { ?> / <?php }?> <span><?php echo get_post_meta($post->ID, 'snbpd_pagedesc', true); ?></span></h2>
                    <div id="search-wrapper-right">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>

            <div id="slider-border-pattern"></div>
            <div id="white-background">
                <!-- content -->
                <div id="content" class="container clearfix">
                    <?php
                    wp_reset_query();
                    wp_reset_postdata();
                    ?>

                    <article>
                        <div class="container clearfix">
                            <?php the_content(); ?>
                        </div>
                    </article>
                    <!-- begin pricing tables section -->
                    <?php if(of_get_option('sc_pricing') == '1') { ?>
                    <div class="container clearfix">
                        <div class="horizontal-line" style="margin-bottom: 70px;"></div>
                    </div>

					<div class="portfolio-container">
                            <div class="pricing-box-list-box ">
                                <?php
                                global $post;
                                $term = get_query_var('term');
                                $tax = get_query_var('taxonomy');
                                $args=array('post_type'=> 'pricing','post_status'=> 'publish', 'orderby'=> 'menu_order', 'caller_get_posts'=>1, 'paged'=>$paged, 'posts_per_page'=>of_get_option('sc_portfolioitemsperpage'));
                                $taxargs = array($tax=>$term);
                                if($term!='' && $tax!='') { $args  = array_merge($args, $taxargs); }

                                query_posts($args);

                                while ( have_posts()):the_post();
                                    $categories = wp_get_object_terms( get_the_ID(), 'jobs');
                                ?>
                                <div class="one-fourth">
                                    <div class="pricing-item" id="<?php global $post; echo get_post_meta($post->ID, 'snbp_best_price', TRUE); ?>" >
                                        <div class="pricing-heading">
                                            <h3><?php the_title(' '); ?></h3>
                                            <div class="pricing-plan-cost">
                                                <h4>
                                                    <?php
                                                    global $post;
                                                    $text = get_post_meta( $post->ID, 'snbp_price', true );
                                                    echo $text;
                                                    ?>
                                                </h4>

                                            </div>
                                            <!-- END pricing-plan-cost -->
                                            <div class="pricing-subtitle">
                                                <?php
                                                global $post;
                                                $text = get_post_meta( $post->ID, 'snbp_subtitle', true );
                                                echo $text;
                                                ?>
                                            </div>
                                        </div>
                                        <div class="pricing-plan-features">
                                            <?php the_content(' '); ?>
                                        </div>
                                        <!-- END pricing-plan-features -->
                                        <div class="pricing-plan-button">
                                            <?php
                                            //check if pricing URL exists and show link
                                            global $post;
                                            $pricing_url = get_post_meta($post->ID, 'snbp_url', true);
                                            $url_title = get_post_meta($post->ID, 'snbp_url_title', true);

                                            if($pricing_url != '') { ?>
                                                <p class="readmore"><a href="<?php echo $pricing_url ?>" title="<?php echo $pricing_url ?>"><?php echo $url_title?> </a></p>
                                            <?php } ?>

                                        </div>
                                    </div>
                                </div>
                                <?php endwhile;  ?>
                            </div>
					</div><?php } ?><!-- end pricing tables section -->
			    </div><!-- end content -->
                <div class="container clearfix">
                    <!-- begin featured clients section -->
                    <?php if(of_get_option('sc_clients') == '1') { ?>
                    <div class="horizontal-line"></div>

                    <div class="container clearfix"  style="padding-bottom: 30px;">
                        <?php above_footer_widget(); //Action hook ?>
                    </div>

                    <?php } ?>
                    <!-- end featured clients section -->
                </div>

            </div><!-- end #white-background -->
<?php get_footer(); ?>