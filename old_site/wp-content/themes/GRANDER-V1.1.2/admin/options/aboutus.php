<?php
    $options[] = array( "name" => "Abouts Us",
    					"sicon" => "aboutus-32x32.png",
						"type" => "heading");

    $options[] = array( "name" => "Display Members on About Us page",
                        "desc" => "do you want to display mebers section on about us page ?",
                        "id" => $shortname."_members",
                        "std" => "1",
                        "type" => "checkbox");

    $options[] = array( "name" => "Members section title",
                        "id" => $shortname."_memberstitle",
                        "std" => "OUR LOVELY TEAM",
                        "type" => "text");
?>