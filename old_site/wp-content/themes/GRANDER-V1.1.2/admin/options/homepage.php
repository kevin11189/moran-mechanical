<?php
	$options[] = array( "name" => "Homepage",
	                    "sicon" => "user-home.png",
	                    "type" => "heading");
					
	$options[] = array( "name" => "Display Blurb on Homepage",
						"id" => $shortname."_blurbhome",
						"std" => "1",
						"type" => "checkbox");
						
	$options[] = array( "name" => "Blurb Content",
                        "id" => $shortname."_blurb",
                        "std" => "HELLO & <strong>WELCOME</strong> TO OUR <strong>GRANDER</strong> WEBSITE",
						"class" => "sectionlast",
                        "type" => "textarea");
						
	$options[] = array( "name" => "Display Content Boxes on Homepage",
						"id" => $shortname."_homecontent",
						"std" => "1",
						"type" => "checkbox");
						
	$options[] = array( "name" => "Content Box 1 Title",
                        "id" => $shortname."_homecontent1title",
                        "std" => "CLIENT SUPPORT",
                        "type" => "text");
						
	$options[] = array( "name" => "Content Box 1 Text",
                        "id" => $shortname."_homecontent1",
                        "std" => "Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus. Nulla dui.",
                        "type" => "textarea");
						
	$options[] = array( "name" => "Content Box 1 Image",
                        "desc" => "Click to 'Upload Image' button and upload Content Box 1 image.",
                        "id" => $shortname."_homecontent1img",
                        "std" => "$blogpath/library/images/sampleimages/featured-icon-01.png",
                        "type" => "upload");
						
	$options[] = array( "name" => "Content Box 1 URL",
                        "id" => $shortname."_homecontent1url",
                        "std" => "#",
						"class" => "sectionlast",
                        "type" => "text");
					
	$options[] = array( "name" => "Content Box 2 Title",
                        "id" => $shortname."_homecontent2title",
                        "std" => "GREAT OPTIONS",
                        "type" => "text");

	$options[] = array( "name" => "Content Box 2 Text",
                        "id" => $shortname."_homecontent2",
                        "std" => "Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus. Nulla dui.",
                        "type" => "textarea");
						
	$options[] = array( "name" => "Content Box 2 Image",
                        "desc" => "Click to 'Upload Image' button and upload Content Box 2 image.",
                        "id" => $shortname."_homecontent2img",
                        "std" => "$blogpath/library/images/sampleimages/featured-icon-02.png",
                        "type" => "upload");
						
	$options[] = array( "name" => "Content Box 2 URL",
                        "id" => $shortname."_homecontent2url",
                        "std" => "#",
						"class" => "sectionlast",
                        "type" => "text");	

	$options[] = array( "name" => "Content Box 3 Title",
                        "id" => $shortname."_homecontent3title",
                        "std" => "REGULAR UPDATES",
                        "type" => "text");
	
	$options[] = array( "name" => "Content Box 3",
                        "id" => $shortname."_homecontent3",
                        "std" => "Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus. Nulla dui.",
                        "type" => "textarea");
						
	$options[] = array( "name" => "Content Box 3 Image",
                        "desc" => "Click to 'Upload Image' button and upload Content Box 3 image.",
                        "id" => $shortname."_homecontent3img",
                        "std" => "$blogpath/library/images/sampleimages/featured-icon-03.png",
                        "type" => "upload");
						
	$options[] = array( "name" => "Content Box 3 URL",
                        "id" => $shortname."_homecontent3url",
                        "std" => "#",
						"class" => "sectionlast",
                        "type" => "text");

    $options[] = array( "name" => "Content Box 4 Title",
                        "id" => $shortname."_homecontent4title",
                        "std" => "RESPONSIVENESS",
                        "type" => "text");

    $options[] = array( "name" => "Content Box 4 Text",
                        "id" => $shortname."_homecontent4",
                        "std" => "Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus. Nulla dui.",
                        "type" => "textarea");

    $options[] = array( "name" => "Content Box 4 Image",
                        "desc" => "Click to 'Upload Image' button and upload Content Box 4 image.",
                        "id" => $shortname."_homecontent4img",
                        "std" => "$blogpath/library/images/sampleimages/featured-icon-04.png",
                        "type" => "upload");

    $options[] = array( "name" => "Content Box 4 URL",
                        "id" => $shortname."_homecontent4url",
                        "std" => "#",
                        "class" => "sectionlast",
                        "type" => "text");

    $options[] = array( "name" => "Display Call to Action Box on Homepage",
                        "desc" => "do you want to display call to action box on homepage ?",
                        "id" => $shortname."_actionbox",
                        "std" => "1",
                        "type" => "checkbox");

    $options[] = array( "name" => "Action Box Title",
                        "id" => $shortname."_actionboxtitle",
                        "std" => "GRANDER IS A CLEAN MULTI-PURPOSE WORDPRESS THEME !",
                        "type" => "text");

    $options[] = array( "name" => "Action Box Text",
                        "id" => $shortname."_actionboxcontent",
                        "std" => "Also you have the ability to change or delete the button text ''Purchase Now'' with everything you want.",
                        "type" => "textarea");

    $options[] = array( "name" => "Action Box Button Title ",
                        "id" => $shortname."_actionboxbuttontitle",
                        "std" => "Purchase Now",
                        "type" => "text");

    $options[] = array( "name" => "Action Box URL",
                        "id" => $shortname."_actionboxurl",
                        "std" => "#",
                        "class" => "sectionlast",
                        "type" => "text");

    $options[] = array( "name" => "Display Portfolio on Homepage",
						"desc" => "do you want to display portfolio section on homepage ?",
						"id" => $shortname."_portfoliohome",
						"std" => "1",
						"type" => "checkbox");
	
	$options[] = array( "name" => "Portfolio section title",
                        "id" => $shortname."_portfoliohometitle",
                        "std" => "RECENT WORKS /",
                        "type" => "text");

    $options[] = array( "name" => "Portfolio section button title",
                        "id" => $shortname."_portfoliohomebuttontitle",
                        "std" => "View Full Portfolio",
                        "type" => "text");

	
	$options[] = array( "name" => "Portfolio section button URL",
                        "id" => $shortname."_portfoliohomebuttonurl",
                        "std" => "#",
                        "type" => "text");

    $options[] = array( "name" => "Display Dinamic Content on Homepage",
                        "desc" => "do you want to display dinamic content on homepage ?",
                        "id" => $shortname."_dinamiccontent",
                        "std" => "1",
                        "type" => "checkbox");

?>