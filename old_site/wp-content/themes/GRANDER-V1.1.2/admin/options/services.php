<?php
    $options[] = array( "name" => "Services",
    					"sicon" => "services-32x32.png",
						"type" => "heading");

    $options[] = array( "name" => "Display Items on Services page",
                        "desc" => "do you want to display services section on services page ?",
                        "id" => $shortname."_services",
                        "std" => "1",
                        "type" => "checkbox");

?>