<?php
    $options[] = array( "name" => "Pricing",
    					"sicon" => "pricing-32x32.png",
						"type" => "heading");

    $options[] = array( "name" => "Display Pricing Tables on Pricing page",
                        "desc" => "do you want to display pricing tables section on pricing page ?",
                        "id" => $shortname."_pricing",
                        "std" => "1",
                        "type" => "checkbox");

?>