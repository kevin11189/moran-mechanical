<?php
/*
* Template Name: 404 Error Page
*/
    get_header(); ?>

            <div class="container">
                <div class="page-title-heading">
                    <h2><?php the_title(); ?><?php if ( !get_post_meta($post->ID, 'snbpd_pagedesc', true)== '') { ?> / <?php }?> <span><?php echo get_post_meta($post->ID, 'snbpd_pagedesc', true); ?></span></h2>
                    <div id="search-wrapper-right">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>

            <div id="slider-border-pattern"></div>
            <div id="white-background">
                <!-- content -->
                <div id="content" class="clearfix">

                    <div id="main" role="main" class="container clearfix">

                        <article id="post-not-found">


                            <header>

                                <h1 class="not-found-text"> · <span>404</span> <?php _e("Error", "site5framework"); ?> · </h1>

                            </header> <!-- end article header -->

                            <section class="post_content">

                                <p align="center"><?php _e("This file may have been moved or deleted. Be sure to check your spelling", "site5framework"); ?></p>

                            </section> <!-- end article section -->

                        </article> <!-- end article -->

                    </div> <!-- end #main -->

                </div> <!-- end #content -->

                <div class="container clearfix">
                    <!-- begin featured clients section -->
                    <?php if(of_get_option('sc_clients') == '1') { ?>
                    <div class="horizontal-line"></div>

                    <div class="container clearfix"  style="padding-bottom: 30px;">
                        <?php above_footer_widget(); //Action hook ?>
                    </div>

                    <?php } ?>
                    <!-- end featured clients section -->
                </div>

            </div><!-- end #white-background -->

<?php get_footer(); ?>
