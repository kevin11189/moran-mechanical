<?php get_header(); ?>

        <div class="container">
            <div class="page-title-heading">
            <h2><?php _e("OUR BLOG", "site5framework"); ?>
                <?php
                $singledescpage = of_get_option('sc_singledesc');
                $singledesc = get_post_meta($singledescpage, 'snbpd_pagedesc');
                ?>

                <?php if (!empty($singledesc)) {
                    echo ' / <span>';
                    echo $singledesc[0].'</span>';
                }?>
            </h2>
            <div id="search-wrapper-right">
                <?php get_search_form(); ?>
            </div>
            </div>
        </div>

        <div id="slider-border-pattern"></div>
        <div id="white-background">
            <!-- content -->
            <div id="content" class="container clearfix">
                <div class="three-fourth">
                    <div id="main" role="main" class="sidebar-line-left">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <!-- begin article -->
                    <?php if ( has_post_format( 'image' ) ) : { ?>
                        <article class="image-post">
                            <?php  get_template_part( 'lib/post-formats/image' ); ?>
                        </article>
                        <?php }	elseif ( has_post_format( 'gallery' ) ) : { ?>
                        <article class="gallery-post">
                            <?php  get_template_part( 'lib/post-formats/gallery' ); ?>
                        </article>
                        <?php }	elseif ( has_post_format( 'video' ) ) : { ?>
                        <article class="video-post">
                            <?php  get_template_part( 'lib/post-formats/video' ); ?>
                        </article>
                        <?php }	elseif ( has_post_format( 'link' ) ) : { ?>
                        <article class="link-post">
                            <?php  get_template_part( 'lib/post-formats/link' ); ?>
                        </article>
                        <?php }	elseif ( has_post_format( 'quote' ) ) : { ?>
                        <article class="quote-post">
                            <?php  get_template_part( 'lib/post-formats/quote' ); ?>
                        </article>
                        <?php }	elseif ( has_post_format( 'aside' ) ) : { ?>
                        <article class="aside-post">
                            <?php  get_template_part( 'lib/post-formats/aside' ); ?>
                        </article>
                        <?php }	else : { ?>
                        <article class="standard-post">
                            <?php  get_template_part( 'lib/post-formats/standard' ); ?>
                        </article>
                        <?php }	endif; ?>
                    <!-- end article -->

                    </div><!-- end #main -->

                    <?php if(of_get_option('sc_authorbox') == '1') : ?>
					<div class="author clearfix">
				        <div class="author-gravatar">
				            <?php echo get_avatar( $post->post_author, 64 ); ?>       
				        </div>
				        <div class="author-about">
				        	<h4>
				        	<?php if(get_the_author_meta( 'first_name') != ''  &&  get_the_author_meta( 'last_name') != '' ) : ?>
				            	<?php the_author_meta( 'first_name'); ?> <?php the_author_meta( 'last_name'); ?> 
				       		<?php else: ?>
				           		<?php the_author_meta( 'user_nicename'); ?> 
				       		<?php endif; ?>
				       		</h4>
				            <p class="author-description"><?php the_author_meta( 'description' ); ?></p>
				        </div>
				    </div>
					<?php endif ?>
                    <div class="horizontal-line"></div>
						
						<?php comments_template(); ?>
						
						<?php endwhile; ?>			
						
						<?php else : ?>
						
						<article id="post-not-found">
						    <header>
						    	<h1><?php _e("No Posts Yet", "site5framework"); ?></h1>
						    </header>
						    <section class="post_content">
						    	<p><?php _e("Sorry, What you were looking for is not here.", "site5framework"); ?></p>
						    </section>
						</article>

						<?php endif; ?>
						
					
				</div><!-- three-fourth -->

				<div class="one-fourth last">
					<?php get_template_part( 'blog', 'sidebar' ); ?>
				</div>
			</div> <!-- end #content -->

            <div class="container clearfix">
                <!-- begin featured clients section -->
                <?php if(of_get_option('sc_clients') == '1') { ?>
                <div class="horizontal-line"></div>

                <div class="container clearfix"  style="padding-bottom: 30px;">
                    <?php above_footer_widget(); //Action hook ?>
                </div>

                <?php } ?>
                <!-- end featured clients section -->
            </div>

        </div><!-- end #white-background -->
<?php get_footer(); ?>