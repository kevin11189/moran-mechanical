<?php 
/*
 * Template Name: Portfolio 3 Cols
 */
get_header(); ?>

            <div class="container">
                <div class="page-title-heading">
                    <h2><?php the_title(); ?><?php if ( !get_post_meta($post->ID, 'snbpd_pagedesc', true)== '') { ?> / <?php }?> <span><?php echo get_post_meta($post->ID, 'snbpd_pagedesc', true); ?></span></h2>
                    <div id="search-wrapper-right">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>

            <div id="slider-border-pattern"></div>
            <div id="white-background">
                <!-- content -->
                <div id="content" class="container clearfix">
                    <!-- begin filterable section -->
                    <?php if(of_get_option('sc_filterableon') == '1') { ?>
					<ul class="filterable" id="<?php echo of_get_option('sc_portfoliofilters')=='javascript' ? 'filterable' : '' ?>">
						<li class="active"><a href="<?php  echo get_permalink( of_get_option('sc_portfoliodesc') ) ?>" data-value="all" data-type="all" class="all"><?php _e('all', 'site5framework'); ?></a></li>
						<?php  
						$categories=  get_categories('taxonomy=types&title_li='); foreach ($categories as $category){ ?>
						<?php //print_r(get_term_link($category->slug, 'types')) ?>
						<li><a href="<?php echo get_term_link($category->slug, 'types') ?>" class="<?php echo $category->category_nicename;?>" data-type="<?php echo $category->category_nicename;?>"><?php echo $category->name;?></a></li>
						<?php }?>

					</ul>
                    <?php } ?><!-- end filterable section -->
					<div class="portfolio-container">

                        <ul id="portfolio-items-one-third"  class="portfolio-items-one-third clearfix">

                            <?php
                            global $post;
                            $term = get_query_var('term');
                            $tax = get_query_var('taxonomy');
                            $args=array('post_type'=> 'portfolio','post_status'=> 'publish', 'orderby'=> 'menu_order', 'caller_get_posts'=>1, 'paged'=>$paged, 'posts_per_page'=>of_get_option('sc_portfolioitemsperpage'));
                            $taxargs = array($tax=>$term);
                            if($term!='' && $tax!='') { $args  = array_merge($args, $taxargs); }

                            query_posts($args);

                            while ( have_posts()):the_post();
                                $categories = wp_get_object_terms( get_the_ID(), 'types');
                                ?>

                                <li class="item <?php foreach ($categories as $category) { echo $category->slug. ' '; } ?>" data-id="id-<?php the_ID(); ?>" data-type="<?php foreach ($categories as $category) { echo $category->slug. ' '; } ?>">
                                    <div class="portfolio-item ">
                                        <div class="item-content">
                                            <div class="link-holder">
                                                <div class="portfolio-item-holder">
                                                    <div class="portfolio-item-hover-content">

                                                        <?php
                                                        $thumbId = get_image_id_by_link ( get_post_meta($post->ID, 'snbp_pitemlink', true) );
                                                        $thumb = wp_get_attachment_image_src($thumbId, 'portfolio-thumbnail', false);
                                                        $large = wp_get_attachment_image_src($thumbId, 'large', false);

                                                        if (!$thumb == ''){ ?>

                                                            <a href="<?php echo $large[0] ?>" title="<?php the_title(); ?>" data-rel="prettyPhoto" class="zoom">View Image</a>
                                                            <a href="<?php the_permalink() ?>" class="full-width-link"></a>
                                                            <img src="<?php echo $thumb[0] ?>" alt="<?php the_title(); ?>"  class="portfolio-img" width="300" />
                                                            <?php } else { ?>
                                                            <img src="<?php echo get_template_directory_uri(); ?>/library/images/sampleimages/portfolio-img.png" alt="<?php the_title(); ?>" width="300"  class="portfolio-img" />
                                                            <?php } ?>

                                                        <div class="hover-options"></div>
                                                    </div>
                                                </div>
                                                <div class="description">
                                                    <p>
                                                        <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                                                    </p>
                                                    <span><?php $separator = ''; foreach ($categories as $category) { echo $separator . $category->name; $separator=' / ';} ?></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endwhile;  ?>

                        </ul>

						<!-- begin #pagination -->
						<?php if (function_exists("wpthemess_paginate")) { wpthemess_paginate(); } ?>
						<!-- end #pagination -->

						<?php 
							wp_reset_query();
							wp_reset_postdata();	
						?>
					</div>
                </div><!-- end content -->

                <div class="container clearfix">
                    <!-- begin featured clients section -->
                    <?php if(of_get_option('sc_clients') == '1') { ?>
                    <div class="horizontal-line"></div>

                    <div class="container clearfix"  style="padding-bottom: 30px;">
                        <?php above_footer_widget(); //Action hook ?>
                    </div>

                    <?php } ?>
                    <!-- end featured clients section -->
                </div>

            </div><!-- end #white-background -->
<?php get_footer(); ?>