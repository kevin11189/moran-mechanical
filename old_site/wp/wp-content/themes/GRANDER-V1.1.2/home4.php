<?php
/*
Template Name: Home4
*/
?>

<?php get_header(); ?>

            <?php if(of_get_option('sc_blurbhome') == '1') { ?>
            <!-- begin .blurb -->
            <div class="intro-page">
                <?php if(!of_get_option('sc_blurb') == '')  { ?>
                <h2><?php echo of_get_option('sc_blurb') ?></h2>
                <?php }?>
            </div>
            <?php } ?>
            <!-- end .blurb -->

            <!-- begin #full-width-slider -->
            <div id="full-width-slider">
                <div class="container clearfix">
                    <?php if(of_get_option('sc_displayslider') == '1') { ?>
                    <?php if(of_get_option('sc_slidertype') == 'rev') { ?>
                        <?php putRevSlider( "homepage" ) ?>
                        <?php } ?>
                    <?php if(of_get_option('sc_slidertype') == 'flex') { ?>
                        <?php get_template_part( 'homepage', 'slider' ); ?>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div id="slider-border-pattern"></div>

            <!-- end #full-width-slider -->
            <!-- begin #white-bg -->
            <div id="white-background" >
                <div id="content" class="clearfix">
                    <div class="container clearfix">
                        <div class="slider-shadow" style="max-width: 940px;"></div>
                    </div>

                    <?php if(of_get_option('sc_homecontent') == '1') { ?>

                    <div class="container clearfix">
                        <div class="horizontal-line"></div>
                    </div>

                    <!--begin cols content -->
                    <div id="three-boxes-left">
                    <div class="container clearfix">
                        <div class="one-fourth">
                            <div class="home-content-icon">
                                <div class="icon-center">
                                    <img src="<?php echo of_get_option('sc_homecontent1img') ?>" class="img-align-left" alt="<?php echo of_get_option('sc_homecontent1title') ?>" />
                                </div>
                            </div>
                            <h4><?php echo of_get_option('sc_homecontent1title') ?></h4>
                            <p><?php echo of_get_option('sc_homecontent1') ?></p>
                            <?php if (of_get_option('sc_homecontent1url')!='') { ?>
                            <p class="readmore">
                                <a href="<?php echo of_get_option('sc_homecontent1url') ?>"><?php _e('Read More', 'site5framework') ?></a>
                            </p>
                            <?php } ?>
                        </div>

                        <div class="one-fourth">
                            <div class="home-content-icon">
                                <div class="icon-center">
                                    <img src="<?php echo of_get_option('sc_homecontent2img') ?>" class="img-align-left" alt="<?php echo of_get_option('sc_homecontent2title') ?>" />
                                </div>
                            </div>
                            <h4><?php echo of_get_option('sc_homecontent2title') ?></h4>
                            <p><?php echo of_get_option('sc_homecontent2') ?></p>
                            <?php if (of_get_option('sc_homecontent2url')!='') { ?>
                            <p class="readmore">
                                <a href="<?php echo of_get_option('sc_homecontent2url') ?>"><?php _e('Read More', 'site5framework') ?></a>
                            </p>
                            <?php } ?>
                        </div>

                        <div class="one-fourth">
                            <div class="home-content-icon">
                                <div class="icon-center">
                                    <img src="<?php echo of_get_option('sc_homecontent3img') ?>" class="img-align-left" alt="<?php echo of_get_option('sc_homecontent3title') ?>" />
                                </div>
                            </div>
                            <h4><?php echo of_get_option('sc_homecontent3title') ?></h4>
                            <p><?php echo of_get_option('sc_homecontent3') ?></p>
                            <?php if (of_get_option('sc_homecontent3url')!='') { ?>
                            <p class="readmore">
                                <a href="<?php echo of_get_option('sc_homecontent3url') ?>"><?php _e('Read More', 'site5framework') ?></a>
                            </p>
                            <?php } ?>
                        </div>

                        <div class="one-fourth last">
                            <div class="home-content-icon">
                                <div class="icon-center">
                                    <img src="<?php echo of_get_option('sc_homecontent4img') ?>" class="img-align-left" alt="<?php echo of_get_option('sc_homecontent4title') ?>" />
                                </div>
                            </div>
                            <h4><?php echo of_get_option('sc_homecontent4title') ?></h4>
                            <p><?php echo of_get_option('sc_homecontent4') ?></p>
                            <?php if (of_get_option('sc_homecontent4url')!='') { ?>
                            <p class="readmore">
                                <a href="<?php echo of_get_option('sc_homecontent4url') ?>"><?php _e('Read More', 'site5framework') ?></a>
                            </p>
                            <?php } ?>
                        </div>
                    </div>
                    </div>
                    <!-- end cols content -->
                    <?php } ?>

                    <?php if(of_get_option('sc_actionbox') == '1') { ?>

                    <div class="container clearfix">
                        <div class="horizontal-line"></div>
                    </div>

                    <!-- begin call to action box -->
                    <div class="container clearfix">
                        <div id="call-to-action-box">
                            <div class="call-to-action-content clearfix">
                                <h4><?php echo of_get_option('sc_actionboxtitle') ?></h4>
                                <p><?php echo of_get_option('sc_actionboxcontent') ?></p>
                            </div>
                            <?php if (of_get_option('sc_actionboxurl')!='') ?>
                            <div class="one_fourth last">
                                <p class="readmore large"><a href="<?php echo of_get_option('sc_actionboxurl') ?>" title="<?php echo of_get_option('sc_actionboxbuttontitle') ?>"><?php echo of_get_option('sc_actionboxbuttontitle') ?></a></p>
                        </div>
                        </div>
                        <div id="call-to-action-shadow"></div>
                    </div>
                    <!-- end call to action box -->
                    <?php } ?>

                    <div class="container clearfix">
                        <div class="horizontal-line"></div>
                    </div>

                    <?php if(of_get_option('sc_portfoliohome') == '1') : ?>
                    <div class="container clearfix">
                        <h4><?php echo of_get_option('sc_portfoliohometitle') ?>  <!-- // optional "view full portfolio" button on homepage featured projects -->
                            <a href="<?php echo of_get_option('sc_portfoliohomebuttonurl') ?>" class="colored" title="<?php echo of_get_option('sc_portfoliohomebuttontitle') ?>"><?php echo of_get_option('sc_portfoliohomebuttontitle') ?></a></h4>
                    </div>
                    <div class="container">
                        <?php
                            $args=array('post_type'=> 'portfolio', 'post_status'=> 'publish','orderby'=> 'menu_order','posts_per_page'=>8,'showposts'=>8,'caller_get_posts'=>1,'paged'=>$paged,); query_posts($args);
                            if ( have_posts() ) :
                                ?>
                                <ul id="projects-carousel" class="loading">
                                    <?php
                                    while (have_posts()): the_post();
                                        $categories = wp_get_object_terms( get_the_ID(), 'types');
                                        ?>
                                        <!-- PROJECT ITEM STARTS -->
                                        <li>
                                            <div class="item-content">
                                                <div class="link-holder">
                                                    <div class="portfolio-item-holder">
                                                        <div class="portfolio-item-hover-content">
                                                            <?php
                                                            $thumbId = get_image_id_by_link ( get_post_meta($post->ID, 'snbp_pitemlink', true) );

                                                            $thumb = wp_get_attachment_image_src($thumbId, 'portfolio-thumbnail', false);
                                                            $large = wp_get_attachment_image_src($thumbId, 'large', false);

                                                            if (!$thumb == ''){ ?>

                                                                <a href="<?php echo $large[0] ?>" title="<?php the_title(); ?>" data-rel="prettyPhoto" class="zoom">View Image</a>
                                                                <a href="<?php the_permalink() ?>" class="full-width-link"></a>
                                                                <img src="<?php echo $thumb[0] ?>" alt="<?php the_title(); ?>" width="220" class="portfolio-img" />
                                                                <?php } else { ?>
                                                                <img src="<?php echo get_template_directory_uri(); ?>/library/images/sampleimages/portfolio-img.jpg" alt="<?php the_title(); ?>" width="220"  class="portfolio-img" />
                                                                <?php }?>

                                                            <div class="hover-options"></div>
                                                        </div>
                                                    </div>
                                                    <div class="description">
                                                        <p>
                                                            <a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"> <?php the_title(); ?> </a>
                                                        </p>
                                                        <span><?php $separator = ''; foreach ($categories as $category) { echo $separator . $category->name; $separator=' / ';} ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <!-- PROJECT ITEM ENDS -->
                                        <?php
                                    endwhile;
                                    wp_reset_query();
                                    ?>
                                </ul>
                                <?php
                            else :
                                ?>

                                <?php
                            endif;
                            ?>

                    </div>
                    <div class="container clearfix">
                        <div class="horizontal-line"></div>
                    </div>
                    <?php endif ?>

                    <?php if(of_get_option('sc_dinamiccontent') == '1') { ?>

                        <article>
                            <div class="container clearfix">
                                <?php the_content(); ?>
                            </div>
                        </article>

                        <div class="container clearfix">
                            <div class="horizontal-line"></div>
                        </div>
                        <?php } ?>

                        <div class="container">
                            <?php above_footer_widget(); //Action hook ?>
                        </div>

                </div><!-- end #content -->
            </div>
            <!-- end #white-background -->
<?php get_footer(); ?>
