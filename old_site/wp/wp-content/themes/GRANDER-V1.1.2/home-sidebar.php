<?php
//Add homepage above footer sidebar area
?>
<?php function add_above_footer_sidebar() {
    if ( is_active_sidebar( 'above-footer' ) ) :

     dynamic_sidebar( 'above-footer' );

        else :

// This content shows up if there are no widgets defined in the backend.
    if(current_user_can('edit_theme_options')) : ?>
    <div class="help">

        <p>
            <?php _e("Please activate some Widgets.", "site5framework"); ?>

            <a href="<?php echo admin_url('widgets.php')?>" class="add-widget"><?php _e("Add Widget", "site5framework"); ?></a>

        </p>

    </div>
    <?php endif ?>

<?php endif; ?>
<?php } ?>
<?php
//create action hook
function above_footer_widget() {
    do_action('above_footer_widget');
}

add_action('above_footer_widget','add_above_footer_sidebar');
?>
