<?php 
/*
 * Template Name: Blog Sidebar Left
*/
get_header(); ?>
        <div class="container">
            <div class="page-title-heading">
                <h2><?php the_title(); ?><?php if ( !get_post_meta($post->ID, 'snbpd_pagedesc', true)== '') { ?> / <?php }?> <span><?php echo get_post_meta($post->ID, 'snbpd_pagedesc', true); ?></span></h2>
                <div id="search-wrapper-right">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>

        <div id="slider-border-pattern"></div>
        <div id="white-background">
            <!-- content -->
            <div id="content" class="container clearfix">

                <div class="one-fourth">
                    <?php get_template_part( 'blog', 'sidebar' ); ?>
                </div>

                <div class="three-fourth last">
					<div id="main" role="main" class="sidebar-line-right">

							<?php
								// WP 3.0 PAGED BUG FIX
								if ( get_query_var('paged') )
								$paged = get_query_var('paged');
								elseif ( get_query_var('page') )
								$paged = get_query_var('page');
								else
								$paged = 1;

								$args = array(
								'post_type' => 'post',
								'paged' => $paged );
								query_posts($args);
							?>

							<?php if (have_posts()) : $count = 0; ?>
							<?php while (have_posts()) : the_post(); $count++; global $post; ?>


                            <!-- begin article -->
                            <?php if ( has_post_format( 'image' ) ) : { ?>
                                <article class="image-post">
                                    <?php  get_template_part( 'lib/post-formats/image' ); ?>
                                </article>
                                <?php }	elseif ( has_post_format( 'gallery' ) ) : { ?>
                                <article class="gallery-post">
                                    <?php  get_template_part( 'lib/post-formats/gallery' ); ?>
                                </article>
                                <?php }	elseif ( has_post_format( 'video' ) ) : { ?>
                                <article class="video-post">
                                    <?php  get_template_part( 'lib/post-formats/video' ); ?>
                                </article>
                                <?php }	elseif ( has_post_format( 'link' ) ) : { ?>
                                <article class="link-post">
                                    <?php  get_template_part( 'lib/post-formats/link' ); ?>
                                </article>
                                <?php }	elseif ( has_post_format( 'quote' ) ) : { ?>
                                <article class="quote-post">
                                    <?php  get_template_part( 'lib/post-formats/quote' ); ?>
                                </article>
                                <?php }	elseif ( has_post_format( 'aside' ) ) : { ?>
                                <article class="aside-post">
                                    <?php  get_template_part( 'lib/post-formats/aside' ); ?>
                                </article>
                                <?php }	else : { ?>
                                <article class="standard-post">
                                    <?php  get_template_part( 'lib/post-formats/standard' ); ?>
                                </article>
                                <?php }	endif; ?>
                            <!-- end article -->


							<?php endwhile; ?>

						<!-- begin #pagination -->
								<?php if (function_exists("wpthemess_paginate")) { wpthemess_paginate(); } ?>
						<!-- end #pagination -->

						<?php else : ?>

						<article id="post-not-found">
						    <header>
						    	<h1><?php _e("No Posts Yet", "site5framework"); ?></h1>
						    </header>
						    <section class="post_content">
						    	<p><?php _e("Sorry, What you were looking for is not here.", "site5framework"); ?></p>
						    </section>
						</article>

						<?php endif; ?>
				
					</div> <!-- end #main -->

				</div><!-- three-fourth -->
			</div> <!-- end #content -->

            <div class="container clearfix">
                <!-- begin featured clients section -->
                <?php if(of_get_option('sc_clients') == '1') { ?>
                <div class="horizontal-line"></div>

                <div class="container clearfix"  style="padding-bottom: 30px;">
                    <?php above_footer_widget(); //Action hook ?>
                </div>

                <?php } ?>
                <!-- end featured clients section -->
            </div>

        </div><!-- end #white-background -->
<?php get_footer(); ?>