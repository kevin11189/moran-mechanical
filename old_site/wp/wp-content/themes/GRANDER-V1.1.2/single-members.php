<?php get_header(); ?>

        <div class="container">
            <div class="page-title-heading">
                <h2><?php the_title(); ?><?php if ( !get_post_meta($post->ID, 'snbpd_pagedesc', true)== '') { ?> / <?php }?> <span><?php echo get_post_meta($post->ID, 'snbpd_pagedesc', true); ?></span></h2>
                <div id="search-wrapper-right">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>

        <div id="slider-border-pattern"></div>
        <div id="white-background">
            <!-- content -->
            <div id="content" class="container clearfix">
                <div class="container">
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                            <div class="three-fourth">
                                <div class="portfolio-image resize">
                                    <?php
                                    $thumbId = get_image_id_by_link ( get_post_meta($post->ID, 'snbp_pitemlink', true) );
                                    $thumb = wp_get_attachment_image_src($thumbId, 'full', false);
                                    $large = wp_get_attachment_image_src($thumbId, 'full', false);

                                    if (!$thumb == ''){ ?>
                                    <a href="<?php echo $large[0] ?>" data-rel="prettyPhoto" title="<?php the_title(); ?>"><img src="<?php echo $thumb[0] ?>" alt="<?php the_title(); ?>"  /></a>
                                    <?php }  ?>
                                </div>
                            </div>

                            <div class="one-fourth last">
                                <div class="container">
                                    <div class="content">
                                        <div id="single-portfolio-pagination">
                                            <div class="project-pagination">
                                                <div class="project-pagination-prev">
                                                    <?php previous_post_link('%link', ''.__('', 'site5framework')) ?>
                                                </div>
                                                <div class="project-pagination-next">
                                                    <?php next_post_link('%link', __('', 'site5framework') . ''); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="page-body clearfix">
                                    <h3>OVERVIEW</h3>
                                    <?php the_content(); ?>
                                </div> <!-- end article section -->
                            </div>

						<?php endwhile; ?>
					</article>

					<?php else : ?>

					<article id="post-not-found">
						<header>
							<h1><?php _e("Not Found", "site5framework"); ?></h1>
						</header>
						<section class="post_content">
							<p><?php _e("Sorry, but the requested resource was not found on this site.", "site5framework"); ?></p>
						</section>
						<footer>
						</footer>
					</article>

					<?php endif; ?>
				</div>
			</div> <!-- end content -->

            <div class="container clearfix">
                <!-- begin featured clients section -->
                <?php if(of_get_option('sc_clients') == '1') { ?>
                <div class="horizontal-line"></div>

                <div class="container clearfix"  style="padding-bottom: 30px;">
                    <?php above_footer_widget(); //Action hook ?>
                </div>

                <?php } ?>
                <!-- end featured clients section -->
            </div>

        </div><!-- end #white-background -->

<?php get_footer(); ?>