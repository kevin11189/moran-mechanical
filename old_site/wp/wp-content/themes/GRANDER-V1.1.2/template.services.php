<?php 
/*
 * Template Name: Services
 */
get_header(); ?>

            <div class="container">
                <div class="page-title-heading">
                    <h2><?php the_title(); ?><?php if ( !get_post_meta($post->ID, 'snbpd_pagedesc', true)== '') { ?> / <?php }?> <span><?php echo get_post_meta($post->ID, 'snbpd_pagedesc', true); ?></span></h2>
                    <!--<div id="search-wrapper-right">
                        <?php get_search_form(); ?>
                    </div>-->
                </div>
            </div>

            <div id="slider-border-pattern"></div>
            <div id="white-background">
                <!-- content -->
                <div id="content" class="container clearfix">
                    <?php
                    wp_reset_query();
                    wp_reset_postdata();
                    ?>

                    <article>
                        <div class="container clearfix">
                            <?php the_content(); ?>
                        </div>
                    </article>
                    <!-- begin services section -->
                    <?php if(of_get_option('sc_services') == '1') { ?>
                    <div class="container clearfix">
                        <div class="horizontal-line"></div>
                    </div>

					<div class="portfolio-container">
                            <ul id="services-items">
                                <?php
                                global $post;
                                $term = get_query_var('term');
                                $tax = get_query_var('taxonomy');
                                $args=array('post_type'=> 'services','post_status'=> 'publish', 'orderby'=> 'menu_order');
                                $taxargs = array($tax=>$term);
                                if($term!='' && $tax!='') { $args  = array_merge($args, $taxargs); }

                                query_posts($args);

                                while ( have_posts()):the_post();
                                    $categories = wp_get_object_terms( get_the_ID(), '');
                                ?>

                                <li class="one-third <?php foreach ($categories as $category) { echo $category->slug. ' '; } ?>" data-id="id-<?php the_ID(); ?>" data-type="<?php foreach ($categories as $category) { echo $category->slug. ' '; } ?>">
                                    <div class="services-items ">
                                        <div class="item-content">
                                            <div class="services-icon">
                                                        <?php
                                                        $thumbId = get_image_id_by_link ( get_post_meta($post->ID, 'snbp_pitemlink', true) );
                                                        $thumb = wp_get_attachment_image_src($thumbId, 'portfolio-thumbnail', false);
                                                        $large = wp_get_attachment_image_src($thumbId, 'large', false);

                                                        if (!$thumb == ''){ ?>

                                                            <img src="<?php echo $thumb[0] ?>" alt="<?php the_title(); ?>" width="50" />
                                                            <?php } else { ?>
                                                            <img src="<?php echo get_template_directory_uri(); ?>/library/images/sampleimages/portfolio-img.png" alt="<?php the_title(); ?>" width="50"/>
                                                            <?php } ?>
                                            </div>

                                            <div class="description">
                                                <h4>
                                                     <?php the_title(); ?>
                                                </h4>

                                                <?php the_content(); ?>

                                                <span><?php $separator = ''; foreach ($categories as $category) { echo $separator . $category->name; $separator=' / ';} ?></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php endwhile;  ?>
                            </ul>
					</div>
                    <?php } ?><!-- end services section -->
			    </div><!-- end content -->
                <div class="container clearfix">
                    <!-- begin featured clients section -->
                    <?php if(of_get_option('sc_clients') == '1') { ?>
                    <div class="horizontal-line"></div>

                    <div class="container clearfix"  style="padding-bottom: 30px;">
                        <?php above_footer_widget(); //Action hook ?>
                    </div>

                    <?php } ?>
                    <!-- end featured clients section -->
                </div>

            </div><!-- end #white-background -->
<?php get_footer(); ?>