<?php
$options[] = array( "name" => "General",
						"sicon" => "advancedsettings.png",
                        "type" => "heading");


      $options[] = array( "name" => "Your Company Logo",
                        "desc" => "You can use your own company logo. Click to 'Upload Image' button and upload your logo.",
                        "id" => $shortname."_clogo",
                        "std" => "$blogpath/library/images/logo.png",
                        "type" => "upload");
						
	$options[] = array( "name" => "Text as Logo",
                        "desc" => "If you don't upload your own company logo, this text will show up in it's place.",
                        "id" => $shortname."_clogo_text",
                        "std" => "Grander",
                        "type" => "text");
	$options[] = array( "name" => "Theme Color Scheme",
                        "id" => $shortname."_colorscheme",
                        "std" => "purple-dark",
                        "type" => "select",
                        "class" => "tiny", //mini, tiny, small
                         "options" => $colorschemes);
	$options[] = array( "name" => "Custom Favicon",
                        "desc" => "You can use your own custom favicon. Click to 'Upload Image' button and upload your favicon.",
                        "id" => $shortname."_custom_shortcut_favicon",
                        "std" => "",
                        "type" => "upload");
    $options[] = array( "name" => "Enable Featured Clients on all pages",
                        "desc" => "By unchecking this the theme will disable featured clients on all pages except homepage.",
                        "id" => $shortname."_clients",
                        "std" => "1",
                        "type" => "checkbox");

?>