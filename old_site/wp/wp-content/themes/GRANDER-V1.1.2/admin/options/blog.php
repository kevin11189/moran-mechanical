<?php
    $options[] = array( "name" => "Blog",
    					"sicon" => "blog.png",
						"type" => "heading");
						
	$options[] = array( "name" => "Choose the defined Blog page",
						"desc" => "The blog item page will use the same title description, if defined, as the selected page.",
						"id" => $shortname."_singledesc",
                        "type" => "select",
                        "options" => $options_pages);
	$options[] = array( "name" => "Display Author Box",
						"desc" => "Show Author box on the Blog Post page.",
						"id" => $shortname."_authorbox",
						"std" => "1",
						"type" => "checkbox");
?>