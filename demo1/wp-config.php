<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'morandemo1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'N1-Q;sh(9BcFA<-#|1a`m8d1Gb+&i{v;tcty2@|H3#NxC0Tem($a]>-xz{_RG5j+');
define('SECURE_AUTH_KEY',  'H|C+Ldk~N:^^_IIrNw,#S++yB9-=7nEWKSld[1lrKwazq4>CRwndrd$J$>2ZhJzj');
define('LOGGED_IN_KEY',    '{&b:yUr0#edvQ!z<)p)Vsx%$S@-@.[1J;G76)5lke7M!Uz.CY)iYg*Qy9|>|!pD&');
define('NONCE_KEY',        ' $g([U!hqBPBBv*(nx*YUk4ap}f8{Vnn+2f8+:{&B#k[)0-ooPGU9D}?;ZR!!Q>6');
define('AUTH_SALT',        'N#MtOBOW|P+>bNmMQj@[WbTO5n>P4i^ro#_?>yMT{Bmp}b(~klQ:v[&)Mj=!c%,p');
define('SECURE_AUTH_SALT', '-{G>)53+KRF%IVy5$b=5j?k:9-LvJJ4-ZR%;k3M2ex5Ue?ug86,.pDe>RW+o#uM+');
define('LOGGED_IN_SALT',   'z]uC7O_1.DDQ)i<!WcgLl$Mb5>}!KD$3J}|s&7H]a?@ZDDNjN0$m4VNV[Ly=1jlF');
define('NONCE_SALT',       '~_+0 i5ikk(_`+<B)[wzdNn&8v-Ng?IR-bm7Uy>b[$2igi.&ZV0Jdkb2(b=#w-dD');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
