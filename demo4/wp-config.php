<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'morandemo4');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'y.Sd.}XLV#>qG6X}okZprDL9js |i#OqqyIB||IK8tucfRI{x%MlDH$yl6^ 9/3K');
define('SECURE_AUTH_KEY',  'sPcK4?od_3RnLAAP+~>7#Oz>.#tum.:5_|^&g#|1Cx1 p=NK=wx2V#0?INIck[%D');
define('LOGGED_IN_KEY',    '&q<_|;x%.z0dd uN#,@?RJ[i8g^e1417g2aJ/4`3o~+?|!ITYB&}Ty8Pnfe`^`yC');
define('NONCE_KEY',        '8$|]%#iTAE#5^t[xQxqepP->{7a4=9JjUmPeDtQKC8&-HM$WeR@M&3l}#=T)7]*`');
define('AUTH_SALT',        ':v}p+tTM%B+zD* qsmlK}Co-gDgs-R+L46VY%DVv]PN|v+|{O_BK&;VTLuF4E~VX');
define('SECURE_AUTH_SALT', '#&n|$*u+uxq3-<-VFUuZq7`q3p$uN2s^T*W!41|(%m)<&j~Nk8D^y[5peW}+ZN}F');
define('LOGGED_IN_SALT',   '8_FOaL^Ey:/e3+imm?O71xj(nP@x>3&Hk9!)dy f<eH}OLu&%b-MSN<(+dH)$|AC');
define('NONCE_SALT',       '`a:+=1`PncY2JJJg}u-.Xi3*s1)PJ5Vc/KulpFBho3WXnLsfB?Qf0kB]A4>3`ZoJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
